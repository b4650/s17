console.log("1. Drink HTML")
console.log("2. Eat Javascript")
console.log("3. Inhale CSS")
console.log("4. Bake Bootstrap")

let tasks = ["1. Drink HTML", "2. Eat Javascript", "3. Inhale CSS", "4. Bake Bootstrap"]
console.log(tasks)
console.log(tasks[2])

let indexOfLastElement = tasks.length -1
console.log(indexOfLastElement);

console.log(tasks.length);

console.log(tasks[4]);

let numbers = ["one","two","three","four"];
console.log(numbers);

numbers[4] = "five"
console.log(numbers);

numbers.push("element");
console.log(numbers);

function pushMethod(element){
	numbers.push(element);
}
pushMethod("six")
pushMethod("seven")
pushMethod("eight")
console.log(numbers);

numbers.pop()
console.log(numbers);

function popMethod(){
	numbers.pop();
}
popMethod();
console.log(numbers);

numbers.shift()
console.log(numbers);

function shiftMethod(){
	numbers.shift();
}
shiftMethod()
console.log(numbers)

numbers.unshift("zero");
console.log(numbers)

function unshiftMethod(element){
	numbers.unshift(element);
}
unshiftMethod("mcdo")
console.log(numbers)

let numbs = [15, 27, 32, 12, 6, 8, 236]
console.log(numbs)

numbs.sort(
	function(a,b){
		return a-b
	}
);
console.log(numbs)

numbs.sort(
	function(a,b){
		return b-a
	}
);
console.log(numbs)

numbs.reverse();
console.log(numbs);

/*
one parameter pure omission - starting what element until end
two parameters: pure omission - how many elements to be removed
three parameters: replacements - replacement for the removed elements
*/

let nums = numbs.splice(4,2,31,11,111)
console.log(numbs)
console.log(nums)

/*slice method - does not affect the original array - WILL COPY THE ARRAY
-first parameter - index where copying will begin
first parameter - index of the first element to be copied
second parameter - will copy until this element (NOT INDEX)

*/

let slicedNums = numbs.slice(2,7)
console.log(numbs)
console.log(slicedNums)


console.log(numbers)
console.log(numbs)
let animals = ["dog", "tiger", "kangaroo", "chicken"]
console.log(animals);

let newConcat = numbers.concat(numbs, animals)
console.log(newConcat)
console.log(numbers)
console.log(numbs)
console.log(animals)

/*
join method - merges the elements inside the array and makes them a string
Argument is the separator, default is comma
*/

let meal = ["rice", "steak", "juice"];
console.log(meal)

newJoin = meal.join("")
console.log(newJoin)

newJoin = meal.join(" ")
console.log(newJoin)

newJoin = meal.join("-")
console.log(newJoin)

/*
To string method - converts the element to string data type

typeOf - determines the data type of  the element after it
*/

console.log(nums);
console.log(typeof nums);

let newString = numbs.toString()
console.log(newString)
console.log(typeof newString)

let countries = ["US", "PH", "JP", "HK", "SG", "PH", "NZ"]
/*
index of - will extract the index of an element from a string
*/

let index = countries.indexOf("PH")
console.log(index)

index = countries.indexOf("AU")
console.log(index)

/*
lastIndexOf - same with index but last place

*/

index = countries.lastIndexOf("PH")
console.log(index)

/*
	Selection control structures
*/

/*if (countries.indexOf("CAN") === -1){
	console.log("Element not existing");
}else{
	console.log("Element exists in the array")
}*/

// Iterators

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]

/*	array.forEach(*/
/*forEach - returns (performs the statement in)each element in the array
	statement/s
	)
*/

days.forEach(
	function(element){
		console.log(element)
	}
);

/*
map - returns a copy of an array from the original which can be manipulated
array.map(
	function(element){
	statement/s
	}
)
*/

let mapDays = days.map(
	function(element){
		return `${element} is the day of the week.`
	}
)
console.log(mapDays)
console.log(days)

/*
filters - filters the elements and COPIES them into another array
*/

console.log(nums)
let newFilter = numbs.filter(
	function (element){
		return element < 50
	}
)
console.log(newFilter)
console.log(numbs)

/*
Includes - checks if an element is included from a string // inside an array

boolean - true or false
*/

let animalIncludes = animals.includes("dog");
console.log(animalIncludes)

/*
every - checks if all the element pass the condition. -- boolean
*/
console.log(nums)
let newEvery = nums.every(
	function(element){
		return(element > 10)
	}
)
console.log(newEvery)

/*
some checks if some of the element pass the condition (at least 1) -- boolean
*/

let newSome = nums.some(
	function(element){
		return (element > 30)
	}
)
console.log(newSome)

nums.push(50)
console.log(nums)

/*
reduce - performs the operation in all of the elements in the array
first parameter - first element
second parameter - last element
*/

let newReduce = nums.reduce(
	function (a,b){
		return a + b
		/*return b - a*/
	}
)
console.log(newReduce)

let average = newReduce/nums.length
console.log(average)

/*
toFixed - sets the number of decimal - as text
parseInt - converts to object and rounds the number to nearest whole number
parseFloat - round the number to nearest target decimal place through the use of toFixed - still as an object
*/

console.log(average.toFixed(2))
console.log(parseInt(average.toFixed(2)))
console.log(parseFloat(average.toFixed(2)))